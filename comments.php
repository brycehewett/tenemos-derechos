<div id="comments">
	<?php if ( post_password_required() ) : ?>
		<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'tenemosderechos' ); ?></p>
		</div>
		<?php return; ?>
	<?php endif; ?>

	<?php if ( comments_open() ) : ?>
	<?php endif; ?>

	<?php if ( ! comments_open() ) : ?>
		<p><?php _e( 'Comments are closed.', 'tenemosderechos' ); ?></p>
	<?php endif; ?>
</div>
