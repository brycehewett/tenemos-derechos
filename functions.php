<?php

	// Add RSS links to <head> section
	automatic_feed_links();

	add_filter('show_admin_bar', '__return_false');

  /*
   * Proper way to enqueue scripts and styles
   */
  function load_dependencies() {

    //Styles
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"),false, false);
    wp_enqueue_script( 'custom-functions', get_template_directory_uri() . '/js/functions-min.js', array(jquery), '1.0', true );
  }


  add_action( 'wp_enqueue_scripts', 'load_dependencies' );

	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    function filter_ptags_on_images($content){
        return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    }

    add_filter('the_content', 'filter_ptags_on_images');

    //Register Widget areas
    if (function_exists('register_sidebar')) {
      register_sidebar(array(
          'name' => 'Right Sidebar',
          'id'   => 'right-sidebar',
          'description'   => 'Widget area for the right sidebar.',
          'before_widget' => '<div id="%1$s" class="widget %2$s">',
          'after_widget'  => '</div>',
          'before_title'  => '<h3>',
          'after_title'   => '</h3>'
      ));
    }

    // custom menu support
    add_theme_support( 'menus' );
    if ( function_exists( 'register_nav_menus' ) ) {
        register_nav_menus(
            array(
              'primary' => 'Primary Menu',
							'secondary' => 'Secondary Menu',
							'mobile-primary' => 'Primary Mobile Menu',
							'mobile-secondary' => 'Secondary Mobile Menu',
              'footer-primary' => 'Footer Primary Menu',
							'footer-secondary' => 'Footer Secondary Menu'
            )
        );
    }

    add_theme_support( 'post-thumbnails' );

    //add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

    function wpe_excerptlength_index($length) {
        return 40;
    }
    function wpe_excerptmore($more) {
        return '...';
    }

    function wpe_excerpt($length_callback='', $more_callback='') {
        global $post;

    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }

    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }

        $output = get_the_excerpt();
        $output = apply_filters('wptexturize', $output);
        $output = apply_filters('convert_chars', $output);
        $output = '<p class="post-excerpt">'.$output.'</p>';
        echo $output;
    }

    /**
     * Improves the caption shortcode with HTML5 figure & figcaption; microdata & wai-aria attributes
     *
     * @param  string $val     Empty
     * @param  array  $attr    Shortcode attributes
     * @param  string $content Shortcode content
     * @return string          Shortcode output
     */
    function jk_img_caption_shortcode_filter($val, $attr, $content = null)
    {
        extract(shortcode_atts(array(
            'id'      => '',
            'align'   => 'aligncenter',
            'width'   => '',
            'caption' => ''
        ), $attr));

        // No caption, no dice... But why width?
        if ( 1 > (int) $width || empty($caption) )
            return $val;

        if ( $id )
            $id = esc_attr( $id );

        // Add itemprop="contentURL" to image - Ugly hack
        $content = str_replace('<img', '<img itemprop="contentURL"', $content);

        return '<figure id="' . $id . '" aria-describedby="figcaption_' . $id . '" class="wp-caption ' . esc_attr($align) . '" itemscope itemtype="http://schema.org/ImageObject" style="width: ' . (0 + (int) $width) . 'px">' . do_shortcode( $content ) . '<figcaption id="figcaption_'. $id . '" class="wp-caption-text" itemprop="description">' . $caption . '</figcaption></figure>';
    }
    add_filter( 'img_caption_shortcode', 'jk_img_caption_shortcode_filter', 10, 3 );

    /**
     * Optional: set 'ot_show_pages' filter to false.
     * This will hide the settings & documentation pages.
     */
    add_filter( 'ot_show_pages', '__return_false' );

    /**
     * Required: set 'ot_theme_mode' filter to true.
     */
    add_filter( 'ot_theme_mode', '__return_true' );

    /**
     * Required: include OptionTree.
     */
    include_once( 'libs/option-tree/ot-loader.php' );

    /**
     * Theme Options
     */
    include_once( 'includes/_theme-options.php' );

    /**
    * Breadcrumbs
    */

    include_once( 'includes/breadcrumb.php' );

		/**
		* Colored Categories
		*/
		include_once( 'includes/colorful-categories/colorful-categories.php' );

		include_once( 'includes/disqus-language.php' );

    /**
    * Exlude pages from search
    */

    function SearchFilter($query) {
        if ($query->is_search) {
            $query->set('post_type', 'post');
        }
        return $query;
        }
        add_filter('pre_get_posts','SearchFilter');

    ?>
