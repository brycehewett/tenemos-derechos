/*

@codekit-prepend "../libs/html5shiv/dist/html5shiv.min.js"

*/

// remap jQuery to $

(function($){
  $(document).ready(function (){

    var deviceSize = $('body').width();

    var sm = 425;
    var md = 768;
    var lg = 992;

    function toggle_menu(){
      $('.responsive-menu').toggleClass('pushed');
      $('.site-wrapper,.top-nav').toggleClass('pushed');
      $('.menu-toggle').find('.icon').toggleClass('ion-navicon-round');
      $('.menu-toggle').find('.icon').toggleClass('ion-close-round');
    }

    function pin_menu() {
      var scrollHeight = document.body.scrollTop;
      if (scrollHeight > 70 && deviceSize > lg) {
        $('.secondary-menu').addClass('pinned');
      } else {
        $('.secondary-menu').removeClass('pinned');
      }
    }

    $('.menu-toggle').click( function(){
      toggle_menu();
    });

    $(".superlink").click(function(){
      window.location=$(this).find("a.teaser-link").attr("href");
      return false;
    });

    pin_menu();

    $(window).scroll(function(){
      pin_menu();
    });

  });
})(jQuery);
