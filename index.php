<?php get_header();?>

  <div class="col-two-thirds">

    <?php

      if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();

      query_posts('posts_per_page='.get_option('posts_per_page').'&paged='. get_query_var('paged'));

      if( have_posts() ):

        while( have_posts() ): the_post();

          include('includes/post-teaser.php');

        endwhile;

        include('includes/post-nav.php');

        wp_reset_query();

      else: ?>

      <h2><?php _e( 'No Posts Found', 'tenemosderechos' ); ?></h2>

  <?php endif;?>

  </div><!-- /.column-two-thirds -->

<?php get_footer(); ?>
