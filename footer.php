        <div class="col-one-third last sidebar">
          <?php get_sidebar()?>

          <footer class="footer">
              <?php $primary_footer_defaults = array(
                'theme_location'  => 'footer-primary',
                'container'       => 'nav',
                'fallback_cb'     => 'false',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 1,);
              wp_nav_menu( $primary_footer_defaults ); ?>

              <?php $secondary_footer_defaults = array(
                'theme_location'  => 'footer-secondary',
                'container'       => 'nav',
                'fallback_cb'     => 'false',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 1,);
              wp_nav_menu( $secondary_footer_defaults ); ?>

          <p class="legal-disclaimer">
            <span><?php _e('Legal Disclaimer', 'tenemosderechos');?></span><br />
            <?php _e('The materials available on this web site are for informational purposes only and not for the purpose of providing legal advice. You should contact a qualified immigration attorney to obtain advice with respect to any particular issue or problem. Use of and access to this Web site or any of the e-mail links contained within the site do not create an attorney-client relationship between Lester Law and the user or browser. The opinions expressed at or through this site are the opinions of the individual author and may not reflect the opinions of Lester Law or any individual attorney.', 'tenemosderechos');?><br />
            <span class="copyright">&copy;<?php echo date("Y");?> Lester Law</span>
          </p>

          </footer>

            <?php wp_footer(); ?>

            <?php
              if ( function_exists( 'ot_get_option' ) ) {
                $google_analytics = ot_get_option( 'google_analytics' );

              if ( ! empty( $google_analytics ) ) {


            echo "<script>
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                    ga('create', '".$google_analytics."', 'auto');
                    ga('send', 'pageview');

                  </script>";

                }

            }?>
        </div><!--.sidebar-->
      </div><!--.content-->
    </div><!--.site-wrapper-->
  </body>
</html>
