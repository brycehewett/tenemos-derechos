<?php
/**
 * Plugin Name: Disqus language
 * Plugin URI: http://www.kadimi.com/en
 * Description: Tell the plugin "Disqus Comment System" to use the site language.
 * Version: 0.5
 * Author: Nabil Kadimi
 * Author Email: nabil@kadimi.com
 * License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * @package kadimi\disqus-language
 */

add_filter( 'disqus_language_filter','cb_disqus_language_filter' );

/**
 * Adds locale to disqus
 */
function cb_disqus_language_filter() {
	return get_closest_disqus_locale( get_locale() );
}

/**
 * Return closest Disqus locale.
 *
 * @param  String $wp_locale WordPress locale.
 * @return String            Disqus locale.
 */
function get_closest_disqus_locale( $wp_locale ) {

	$wp_languages = array(
		'af' => 'Afrikaans',
		'ak' => 'Akan',
		'sq' => 'Albanian',
		'arq' => 'Algerian Arabic',
		'am' => 'Amharic',
		'ar' => 'Arabic',
		'hy' => 'Armenian',
		'rup_MK' => 'Aromanian',
		'frp' => 'Arpitan',
		'as' => 'Assamese',
		'ast' => 'Asturian',
		'az' => 'Azerbaijani',
		'az_TR' => 'Azerbaijani (Turkey)',
		'bcc' => 'Balochi Southern',
		'ba' => 'Bashkir',
		'eu' => 'Basque',
		'bel' => 'Belarusian',
		'bn_BD' => 'Bengali',
		'bs_BA' => 'Bosnian',
		'bre' => 'Breton',
		'bg_BG' => 'Bulgarian',
		'ca' => 'Catalan',
		'bal' => 'Catalan (Balear)',
		'ceb' => 'Cebuano',
		'zh_CN' => 'Chinese (China)',
		'zh_HK' => 'Chinese (Hong Kong)',
		'zh_TW' => 'Chinese (Taiwan)',
		'co' => 'Corsican',
		'hr' => 'Croatian',
		'cs_CZ' => 'Czech',
		'da_DK' => 'Danish',
		'dv' => 'Dhivehi',
		'nl_NL' => 'Dutch',
		'nl_BE' => 'Dutch (Belgium)',
		'dzo' => 'Dzongkha',
		'art_xemoji' => 'Emoji',
		'en_AU' => 'English (Australia)',
		'en_CA' => 'English (Canada)',
		'en_NZ' => 'English (New Zealand)',
		'en_ZA' => 'English (South Africa)',
		'en_GB' => 'English (UK)',
		'eo' => 'Esperanto',
		'et' => 'Estonian',
		'fo' => 'Faroese',
		'fi' => 'Finnish',
		'fr_BE' => 'French (Belgium)',
		'fr_CA' => 'French (Canada)',
		'fr_FR' => 'French (France)',
		'fy' => 'Frisian',
		'fur' => 'Friulian',
		'fuc' => 'Fulah',
		'gl_ES' => 'Galician',
		'ka_GE' => 'Georgian',
		'de_DE' => 'German',
		'de_CH' => 'German (Switzerland)',
		'el' => 'Greek',
		'kal' => 'Greenlandic',
		'gn' => 'Guaraní',
		'gu' => 'Gujarati',
		'hat' => 'Haitian Creole',
		'haw_US' => 'Hawaiian',
		'haz' => 'Hazaragi',
		'he_IL' => 'Hebrew',
		'hi_IN' => 'Hindi',
		'hu_HU' => 'Hungarian',
		'is_IS' => 'Icelandic',
		'ido' => 'Ido',
		'id_ID' => 'Indonesian',
		'ga' => 'Irish',
		'it_IT' => 'Italian',
		'ja' => 'Japanese',
		'jv_ID' => 'Javanese',
		'kab' => 'Kabyle',
		'kn' => 'Kannada',
		'kk' => 'Kazakh',
		'km' => 'Khmer',
		'kin' => 'Kinyarwanda',
		'ky_KY' => 'Kirghiz',
		'ko_KR' => 'Korean',
		'ckb' => 'Kurdish (Sorani)',
		'lo' => 'Lao',
		'lv' => 'Latvian',
		'li' => 'Limburgish',
		'lin' => 'Lingala',
		'lt_LT' => 'Lithuanian',
		'lb_LU' => 'Luxembourgish',
		'mk_MK' => 'Macedonian',
		'mg_MG' => 'Malagasy',
		'ms_MY' => 'Malay',
		'ml_IN' => 'Malayalam',
		'mri' => 'Maori',
		'mr' => 'Marathi',
		'xmf' => 'Mingrelian',
		'mn' => 'Mongolian',
		'me_ME' => 'Montenegrin',
		'ary' => 'Moroccan Arabic',
		'my_MM' => 'Myanmar (Burmese)',
		'ne_NP' => 'Nepali',
		'nb_NO' => 'Norwegian (Bokmål)',
		'nn_NO' => 'Norwegian (Nynorsk)',
		'oci' => 'Occitan',
		'ory' => 'Oriya',
		'os' => 'Ossetic',
		'ps' => 'Pashto',
		'fa_IR' => 'Persian',
		'fa_AF' => 'Persian (Afghanistan)',
		'pl_PL' => 'Polish',
		'pt_BR' => 'Portuguese (Brazil)',
		'pt_PT' => 'Portuguese (Portugal)',
		'pa_IN' => 'Punjabi',
		'rhg' => 'Rohingya',
		'ro_RO' => 'Romanian',
		'roh' => 'Romansh Vallader',
		'ru_RU' => 'Russian',
		'rue' => 'Rusyn',
		'sah' => 'Sakha',
		'sa_IN' => 'Sanskrit',
		'srd' => 'Sardinian',
		'gd' => 'Scottish Gaelic',
		'sr_RS' => 'Serbian',
		'szl' => 'Silesian',
		'snd' => 'Sindhi',
		'si_LK' => 'Sinhala',
		'sk_SK' => 'Slovak',
		'sl_SI' => 'Slovenian',
		'so_SO' => 'Somali',
		'azb' => 'South Azerbaijani',
		'es_AR' => 'Spanish (Argentina)',
		'es_CL' => 'Spanish (Chile)',
		'es_CO' => 'Spanish (Colombia)',
		'es_GT' => 'Spanish (Guatemala)',
		'es_MX' => 'Spanish (Mexico)',
		'es_PE' => 'Spanish (Peru)',
		'es_PR' => 'Spanish (Puerto Rico)',
		'es_ES' => 'Spanish (Spain)',
		'es_VE' => 'Spanish (Venezuela)',
		'su_ID' => 'Sundanese',
		'sw' => 'Swahili',
		'sv_SE' => 'Swedish',
		'gsw' => 'Swiss German',
		'tl' => 'Tagalog',
		'tah' => 'Tahitian',
		'tg' => 'Tajik',
		'tzm' => 'Tamazight (Central Atlas)',
		'ta_IN' => 'Tamil',
		'ta_LK' => 'Tamil (Sri Lanka)',
		'tt_RU' => 'Tatar',
		'te' => 'Telugu',
		'th' => 'Thai',
		'bo' => 'Tibetan',
		'tir' => 'Tigrinya',
		'tr_TR' => 'Turkish',
		'tuk' => 'Turkmen',
		'twd' => 'Tweants',
		'ug_CN' => 'Uighur',
		'uk' => 'Ukrainian',
		'ur' => 'Urdu',
		'uz_UZ' => 'Uzbek',
		'vi' => 'Vietnamese',
		'wa' => 'Walloon',
		'cy' => 'Welsh',
		'yor' => 'Yoruba ',
	);

	$disqus_languages = array(
		'af' => 'Afrikaans',
		'sq' => 'Albanian',
		'ar' => 'Arabic',
		'hy' => 'Armenian',
		'ast' => 'Asturian',
		'az' => 'Azerbaijani',
		'eu' => 'Basque',
		'bs' => 'Bosnian',
		'bg' => 'Bulgarian',
		'ca_ES' => 'Catalan (Spain)',
		'zh' => 'Chinese',
		'zh_TW' => 'Chinese (Taiwan)',
		'hr' => 'Croatian',
		'cs' => 'Czech',
		'da' => 'Danish',
		'dv' => 'Divehi',
		'nl' => 'Dutch',
		'en' => 'English',
		'et' => 'Estonian',
		'fi' => 'Finnish',
		'fr' => 'French',
		'gl' => 'Galician',
		'de' => 'German',
		'el' => 'Greek',
		'he' => 'Hebrew',
		'hi' => 'Hindi',
		'hu' => 'Hungarian',
		'is' => 'Icelandic',
		'id' => 'Indonesian',
		'it' => 'Italian',
		'ja' => 'Japanese',
		'pam' => 'Kapampangan',
		'kk' => 'Kazakh',
		'km' => 'Khmer',
		'ko' => 'Korean',
		'lad' => 'Ladino',
		'lv' => 'Latvian',
		'lt' => 'Lithuanian',
		'mk' => 'Macedonian',
		'ms' => 'Malay',
		'mr' => 'Marathi',
		'mn' => 'Mongolian',
		'nb' => 'Norwegian Bokmål',
		'nn' => 'Norwegian Nynorsk',
		'fa' => 'Persian',
		'pl' => 'Polish',
		'pt' => 'Portuguese',
		'pt_BR' => 'Portuguese (Brazil)',
		'ro' => 'Romanian',
		'ru' => 'Russian',
		'sc' => 'Sardinian',
		'sr' => 'Serbian',
		'sr@latin' => 'Serbian (Latin)',
		'sk' => 'Slovak',
		'sl' => 'Slovenian',
		'so' => 'Somali',
		'es_AR' => 'Spanish (Argentina)',
		'es_CO' => 'Spanish (Colombia)',
		'es_MX' => 'Spanish (Mexico)',
		'es_ES' => 'Spanish (Spain)',
		'sw' => 'Swahili',
		'sv_SE' => 'Swedish (Sweden)',
		'th' => 'Thai',
		'tr' => 'Turkish',
		'tk' => 'Turkmen',
		'uk' => 'Ukrainian',
		'ur' => 'Urdu',
		'vec' => 'Venetian',
		'vi' => 'Vietnamese',
		'cy' => 'Welsh',
	);

	// Case: Exact match.
	if ( array_key_exists( $wp_locale, $disqus_languages ) ) {
		return $wp_locale;
	}

	// Case: lc_LC to lc.
	if ( 5 == strlen( $wp_locale ) ) {
		$wp_locale = substr( $wp_locale, 0,2 );
		if ( array_key_exists( $wp_locale, $disqus_languages ) ) {
			return $wp_locale;
		}
	}

	// Case: lc_LC to lc.
	if ( 2 == strlen( $wp_locale ) ) {
		foreach ( $disqus_languages as $disqus_locale => $disqus_lang ) {
			if ( 5 == strlen( $disqus_locale ) && substr( $disqus_locale, 0,2 ) === $wp_locale ) {
				return $disqus_locale;
			}
		}
	}
}
