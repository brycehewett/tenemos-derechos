<article class="post-teaser card superlink">

  <header>
    <?php include (TEMPLATEPATH . '/includes/meta.php' ); ?>
    <h2><?php the_title(); ?></h2>
  </header>

  <?php wpe_excerpt('wpe_excerptlength_index', 'wpe_excerptmore'); ?>

  <a class="teaser-link" href="<?php the_permalink() ?>"><?php _e( 'Read More...', 'tenemosderechos' ); ?></a>

</article>
