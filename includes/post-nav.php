<div class="post-nav">
	<div class="prev-posts">
		<?php if (is_single()) {
			previous_post_link('%link', '<i class="icons ion-ios-arrow-back"></i> ' . __('Previous Post', 'tenemosderechos'));
		} else {
			next_posts_link('<i class="icons ion-ios-arrow-back"></i> ' . __('Older Posts', 'tenemosderechos'));
		}?>
	</div><!--.older-->
	<div class="next-posts">
		<?php if (is_single()) {
			next_post_link('%link', __('Next Post', 'tenemosderechos') . ' <i class="icons ion-ios-arrow-forward"></i>');
		} else {
			previous_posts_link(__('Newer Posts', 'tenemosderechos') . ' <i class="icons ion-ios-arrow-forward"></i>');
		} ?>
	</div><!--.older-->
</div>
