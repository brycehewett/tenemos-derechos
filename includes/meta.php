<div class="post-meta">
<?php
  $categories = get_the_category();
  foreach($categories as $category) {
    echo '<a style=" background-color: ' . get_term_meta($category->term_id, 'cc_color', true) . ';" class="post-category-label" href="' . get_term_link($category->term_id) . '">' . $category->name . '</a>';
  }
  echo '<time datetime="' . date(DATE_W3C) . '">';
  the_time('F j, Y');
  echo '</time>';
?>
</div>
