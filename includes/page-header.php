<?php if(is_front_page()) { ?>
	<div class="header-image">
		<div class="container">
		  <div class="header-text">
		    <h1><?php _e('Some people are undocumented.','tenemosderechos');?><br /><strong><?php _e('No one is illegal.','tenemosderechos');?></strong></h1>
		  </div>
		</div>
	</div>
<?php } ?>
