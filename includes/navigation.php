<header class="top-nav">
    <div class="primary-menu">
      <div class="container">
        <a class="logo" href="<?php echo pll_home_url(); ?>" alt="<?php bloginfo('name'); ?>">
          <img src="<?php echo get_template_directory_uri() . '/images/logo.svg';?>"/>
        </a>
        <div class="menu-toggle"><i class="icon ion-navicon-round"></i></div>
        <?php $defaults = array(
          'theme_location'  => 'primary',
          'container'       => 'nav',
          'echo'            => true,
          'fallback_cb'     => 'false',
          'items_wrap'      => '<ul>%3$s</ul>',
          'depth'           => 2,);
        wp_nav_menu( $defaults ); ?>
      </div>
    </div>
    <div class="secondary-menu">
      <div class="container">
        <?php $defaults = array(
          'theme_location'  => 'secondary',
          'container'       => 'nav',
          'echo'            => true,
          'fallback_cb'     => 'false',
          'items_wrap'      => '<ul>%3$s</ul>',
          'depth'           => 1,);
        wp_nav_menu( $defaults ); ?>
      </div>
    </div>
</header>
