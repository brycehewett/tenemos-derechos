<nav class="responsive-menu">
  <a class="logo" href="<?php echo pll_home_url(); ?>" alt="<?php bloginfo('name'); ?>">
    <img src="<?php echo get_template_directory_uri() . '/images/logo.svg';?>"/>
  </a>
  <?php $primary_footer_defaults = array(
    'theme_location'  => 'mobile-primary',
    'container'       => '',
    'fallback_cb'     => 'false',
    'items_wrap'      => '<ul>%3$s</ul>',
    'depth'           => 1,);
  wp_nav_menu( $primary_footer_defaults ); ?>

  <?php $secondary_footer_defaults = array(
    'theme_location'  => 'mobile-secondary',
    'container'       => '',
    'fallback_cb'     => 'false',
    'items_wrap'      => '<ul>%3$s</ul>',
    'depth'           => 1,);
  wp_nav_menu( $secondary_footer_defaults ); ?>
</nav>
