<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();
  include('includes/page-header.php') ?>


    <article class="col-two-thirds card">
      <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>

      <header>
        <h1><?php the_title(); ?></h1>

      </header>

  		<?php the_content(); ?>
      <?php endwhile; endif; ?>

  	</article>



<?php get_footer(); ?>
