<?php get_header(); ?>
  <article class="col-two-thirds card">
    <header>
      <h1>404</h1>
    </header>
    <p><?php _e( 'Page Not Found', 'tenemosderechos' ); ?></p>
  </article>
<?php get_footer(); ?>
