<?php get_header(); ?>

	<div class="col-two-thirds card">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<header>
			<?php include (TEMPLATEPATH . '/includes/meta.php' );?>
    	<h1><?php the_title(); ?></h1>
  	</header>

  	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); }

		the_content(); ?>

		<footer>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php the_tags( 'Tags: ', ', ', ''); ?>
		</footer>
	</article>

	<?php include(TEMPLATEPATH . '/includes/post-nav.php');?>

	<section id="comments">
		<?php comments_template( '', true ); ?>
	</section>

	<?php endwhile; endif; ?>
	</div><!--/.column-two-thirds-->

<?php get_footer(); ?>
