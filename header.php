<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
			<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
			<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<?php if ( function_exists( 'ot_get_option' ) ) {
  	$favicon = ot_get_option( 'favicon' );

			if ( ! empty( $favicon ) ) {
  		echo '<link rel="shortcut icon" href="'.$favicon.'">';
  		}

	}?>
	<!-- This is the traditional favicon.
		 - size: 16x16 or 32x32
		 - transparency is OK
		 - see wikipedia for info on browser support: http://mky.be/favicon/ -->

	<?php if ( function_exists( 'ot_get_option' ) ) {
  	$apple_touch_icon = ot_get_option( 'apple_touch_icon' );

  		if ( ! empty( $apple_touch_icon ) ) {
	  	echo '<link rel="apple-touch-icon" href="'.$apple_touch_icon.'">';
  		}

	}?>
	<!-- The is the icon for iOS's Web Clip.
		 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
		 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
		 - Transparency is not recommended (iOS will put a black BG behind the icon) -->

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>


	<?php
		if ( function_exists( 'ot_get_option' ) ) {
			$facebook_app_id = ot_get_option( 'facebook_app_id' );
			$default_img = ot_get_option( 'default_facebook_logo' );
	 		$thumb = get_post_meta($post->ID,'_thumbnail_id',false);
			$thumb = wp_get_attachment_image_src($thumb[0], 'thumbnail', false);
			$thumb = $thumb[0];
		if ( ! empty( $facebook_app_id ) ) {

			echo '<meta property="fb:app_id" content="'.$facebook_app_id.'"/>';

		if ( is_single() || is_page()) { ?>

			<meta property="og:type" content="article" />
			<meta property="og:title" content="<?php single_post_title(''); ?>" />
			<meta property="og:description" content="<?php
				while(have_posts()):the_post();
				$out_excerpt = str_replace(array("\r\n", "\r", "\n"), "", get_the_excerpt());
				echo apply_filters('the_excerpt_rss', $out_excerpt);
				endwhile; 	?>" />
			<meta property="og:url" content="<?php the_permalink(); ?>"/>
			<meta property="og:image" content="<?php if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
			<?php  } else { ?>
				<meta property="og:type" content="article" />
				<meta property="og:title" content="<?php bloginfo('name'); ?>" />
				<meta property="og:url" content="<?php bloginfo('url'); ?>"/>
				<meta property="og:description" content="<?php bloginfo('description'); ?>" />
	    	<meta property="og:image" content="<?php  if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
			<?php	}

		}
  	} ?>

	</head>

	<body <?php body_class();?>>

	<?php include('includes/responsive-menu.php');?>

	<div class="site-wrapper">

		<!--<?php echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId='.$facebook_app_id.'&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, "script", "facebook-jssdk"));</script>';?>-->

		<?php include('includes/navigation.php'); ?>
		<?php include('includes/page-header.php'); ?>

 		<div class="content container">
