<form action="<?php bloginfo('siteurl'); ?>" class="searchform" method="get">
    <div>
        <label for="s" class="screen-reader-text hidden"><?php _e( 'Search for:', 'tenemosderechos' ); ?></label>
        <input type="search" class="s" name="s" value="" />

        <button type="submit" class="searchsubmit button red" /><i class="fa fa-search"></i></button>
    </div>
</form>
